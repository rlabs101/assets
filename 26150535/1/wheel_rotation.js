var WheelRotation = pc.createScript('wheelRotation');

// initialize code called once per entity
WheelRotation.prototype.initialize = function() {
    
};

// update code called every frame
WheelRotation.prototype.update = function(dt) {
    
    if (this.app.keyboard.isPressed(pc.KEY_SPACE)) {
        this.entity.rotateLocal(-20, 0, 0);
    }
};

// swap method called for script hot-reloading
// inherit your script state here
// WheelRotation.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/