var Retry = pc.createScript('retry');

// initialize code called once per entity
Retry.prototype.initialize = function() {
    this.check = false;
};

// update code called every frame
Retry.prototype.update = function(dt) {
    this.entity.element.on('click', function (event) {
        //alert('check');
        if (this.check === false) {
            window.location.reload();
            this.check = true;
        }
    }, this);
};

// swap method called for script hot-reloading
// inherit your script state here
// Retry.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/